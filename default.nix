let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-20.03-2020-07-18";
    url = "https://github.com/nixos/nixpkgs/archive/2b417708c282d84316366f4125b00b29c49df10f.tar.gz";
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "0426qaxw09h0kkn5zwh126hfb2j12j5xan6ijvv2c905pqi401zq";
  }) {};
in
with pkgs.python37Packages;
  buildPythonPackage rec {
    pname = "discord-talkingstick-bot";
    version = "0.1.0";
    src = ./.;
    # src = builtins.fetchGit {
    #   url = "git://gitlab.com/d6e/discord-speakingstick.git";
    #   ref = "master";
      #rev = "a9a4cd60e609ed3471b4b8fac8958d009053260d";
    # };
    propagatedBuildInputs = [ discordpy ];
    checkPhase = ''
      python -m unittest tests/*.py
    '';
    meta = {
      description = ''
        A discord bot for one-person-at-a-time communication in discord voice chats.
      '';
    };
  }