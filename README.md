## setup
1. Create a new app at https://discord.com/developers then create a bot and set the environment variable `DISCORD_BOT_SECRET` to the bot's secret token. 
2. Create a role with permissions on the server for any permissions the bot requests.
3. `https://discord.com/oauth2/authorize?client_id={client_id}&scope=bot&permissions=4202496`

## Features
- [ ] timed talking stick
  * with 30 sec warning
  * command line timer parameter
- [ ] next member command

