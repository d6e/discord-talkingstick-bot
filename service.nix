{ config, lib, pkgs, ... }:

let
    # The package itself. It resolves to the package installation directory.
    discordTalkingStickBot = import ./default.nix;

    # An object containing user configuration (in /etc/nixos/configuration.nix)
    cfg = config.services.discordTalkingStickBot;
in {
    # Create the main option to toggle the service state
    options.services.discordTalkingStickBot.enable = lib.mkEnableOption "discordTalkingStickBot";

    # The following are the options we enable the user to configure for this
    # package.
    # These options can be defined or overriden from the system configuration
    # file at /etc/nixos/configuration.nix
    # The active configuration parameters are available to us through the `cfg`
    # expression.
    options.services.discordTalkingStickBot.secretDiscordToken = lib.mkOption {
        type = lib.types.str;
        default = "";
    };
    options.services.discordTalkingStickBot.extraArgs = lib.mkOption {
        type = lib.types.listOf lib.types.str;
        default = [""];
        example = ["--debug"];
    };

    # Everything that should be done when/if the service is enabled
    config = lib.mkIf cfg.enable {
        # Open selected port in the firewall.
        # We can reference the port that the user configured.
        # networking.firewall.allowedTCPPorts = [ cfg.port ];

        # Describe the systemd service file
        systemd.services.discord-talkingstick-bot = {
            description = "A discord bot for one-person-at-a-time communication in discord voice chats.";
            environment = {
                PYTHONUNBUFFERED = "1";
                DISCORD_BOT_SECRET = cfg.secretDiscordToken;
            };

            # Wait not only for network configuration, but for it to be online.
            # The functionality of this target is dependent on the system's
            # network manager.
            # Replace the below targets with network.target if you're unsure.
            after = [ "network-online.target" ];
            wantedBy = [ "network-online.target" ];

            # Many of the security options defined here are described
            # in the systemd.exec(5) manual page
            # The main point is to give it as few privileges as possible.
            # This service should only need to talk HTTP on a high numbered port
            # -- not much more.
            serviceConfig = {
                Type = "simple";
                DynamicUser = "true";
                # See how we can reference the installation path of the package,
                # along with all configured options.
                # The package expression `discordTalkingStickBot` expands to the root
                # installation path.
                ExecStart = "${discordTalkingStickBot}/bin/bot.py ${lib.concatStringsSep " " cfg.extraArgs}";
                ExecStop = "${pkgs.procps}/bin/pkill --signal SIGINT bot.py";
                TimeoutStartSec = 30;
                Restart = "always";
                RestartSec = "5";
            };
        };
    };
}
